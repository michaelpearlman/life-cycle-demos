package com.reltio.lca;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.io.Files;
import com.reltio.lifecycle.server.core.services.LifeCycleHook;
import com.reltio.lifecycle.test.LifecycleExecutor;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class BeforeSaveActionSampleTest {
    @Test
    public void test() throws URISyntaxException, IOException {
        BeforeSaveActionSample handler = new BeforeSaveActionSample();
        LifecycleExecutor executor = new LifecycleExecutor();
        String input = Files.toString(new File(getClass().getResource("/input.json").toURI()), Charset.defaultCharset());

        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get("Name").elements().next().get("value").asText();
        assertEquals("John Snow", name);
    }
}
